# Anexos

**Identificación y clasificación de las rutas metabólicas de las especies de *Leptospira* spp.**

+ Genomas: Listado de genomas utilizados y caracteristicas de los mismos
+ Medio: Medio de cultivo EMJH simulado
+ Modelos: Modelos metabólicos a escala genómica de cada uno de los clados en formato XML

```mermaid
graph TD;
    Anexos --> Genomas
    Anexos --> Medio
    Anexos --> Modelos
    Genomas --> Genomas.csv
    Medio --> EMJH.csv
    Modelos --> P1.xml
    Modelos --> P2.xml
    Modelos --> S1-1.xml
    Modelos --> S1-2.xml
    Modelos --> S2.xml
```

Los genomas en formato fasta se encuentran disponibles en: http://fveyrier.profs.inrs.ca/Download/Dataset.zip y fueron publicados por: Vincent A, Schiettekatte O, Goarant C, Neela V, Bernet E, Thibeaux R, et al. Revisiting the taxonomy and evolution of pathogenicity of the genus Leptospira through the prism of genomics. PLoS Negl Trop Dise. 2019 May;13(5):e0007270. Available from: https://doi.org/10.1371/journal.pntd.0007270.